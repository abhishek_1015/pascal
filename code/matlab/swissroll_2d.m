%
% Generate data on a 2d swiss roll
% June 10, 2015
% Created by Abhishek K Dubey
clear;
close all;

step=0.1; max=50;

r=40*step:step:max;

n=max/step;

theta=r;

x=r.*sin(theta);
y=r.*(cos(theta));

dy = gradient(y);
dx = gradient(x);

%normalizing
dy = dy./sqrt(dy.^2+dx.^2);
dx = dx./sqrt(dy.^2+dx.^2);

axis equal;
quiver(x,y, -dy, dx);
hold on;
plot(x,y);

% add 