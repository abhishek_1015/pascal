% Comments
% June 2, 2015  -  Returns the subsequences 
% Created by Abhishek K Dubey

function [num_of_subseq, subseq] = find_subsequences(seq)
num_of_subseq=0;
seq = sort(unique(seq));
begin_idx=1;
subseq_length=0;
for i=1:length(seq) 
  if((i~=begin_idx && seq(i)~=seq(i-1)+1) && (i~=length(seq)))
      num_of_subseq=num_of_subseq+1;
      subseq{num_of_subseq}= seq(begin_idx:begin_idx+subseq_length-1);
      begin_idx=i;
      subseq_length=1;
  elseif((i==length(seq) && seq(i)==seq(i-1)+1))
      num_of_subseq=num_of_subseq+1;
      subseq_length=subseq_length+1;
      subseq{num_of_subseq}= seq(begin_idx:begin_idx+subseq_length-1);
  elseif((i==length(seq) && seq(i)~=seq(i-1)+1))
        num_of_subseq=num_of_subseq+1;
        subseq{num_of_subseq}= seq(begin_idx:begin_idx+subseq_length-1);
        num_of_subseq=num_of_subseq+1;
        subseq{num_of_subseq}=seq(i);
  else
      subseq_length=subseq_length+1;
  end
end
