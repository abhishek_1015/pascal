% Comments:
% June 2, 2015 - locating pseudo spectra
% Created by Abhishek K Dubey

clear; close all;
cmap = hsv(10);
grid_length = {'100', '200'};
distribution = {'Gaussian', 'Uniform'};
noise = {'10-1', '10-2', '10-3'};
data_dir='../../data/';
step=[.03, .015];

grid_length_scheme = input(...
    'Press 1 for grid length 100\nPress 2 for grid length 200\n? ');
distribution_scheme = input(...
    'Press 1 for Gaussian \nPress 2 for Uniform\n? ');
noise_scheme = input(...
    ['Press 1 for Noise level 10^-1\nPress 2 for noise level ' ...
    '10^-2\nPress 3 for noise level 10^-3\n? ']);
%trial_number = input(...
%    ['Press trial number ']);

trial_numbers = [1 2 3];
data_subdir = strcat(grid_length{grid_length_scheme}, '_',...
    distribution{distribution_scheme}, '_', noise{noise_scheme});
filename='/Data_p_spectra_sigm_matrix.txt'
data=load(strcat(data_dir, data_subdir, filename));
data=reshape(data, str2num(grid_length{grid_length_scheme})+1,...
    str2num(grid_length{grid_length_scheme})+1,10);

pevT=zeros(18, 19);

idx=0;
for tau=[.08, .04, 0.02, 0.01, .005, .0025]
       figure;
       view(3);
       hold on;
    for trial_number=trial_numbers
        idx=idx+1;
        pevT(idx,1)=tau;
        pevT(idx,2)=trial_number;
        pseudo_eigenval = NaN(str2num(grid_length{grid_length_scheme})+1, ...
            str2num(grid_length{grid_length_scheme})+1);
        z=reshape(data(:,:,trial_number), str2num(grid_length{grid_length_scheme})+1,...
            str2num(grid_length{grid_length_scheme})+1);
        [x,y] = meshgrid(-1.5:step(grid_length_scheme):1.501);
        [x_idx, y_idx]= find(z<tau);
        
        %plot the pseudoeigenvalue
        pseudo_eigenval(x_idx, y_idx)=z(x_idx, y_idx);
        scatter3(x(:),y(:),pseudo_eigenval(:), 5, cmap(trial_number,:));
        xlabel('Real');
        ylabel('Imag');
        zlabel('Min of SinVal');
        title(strcat( ' trials#', num2str(trial_number)));
        
        %plot the pseudoeigenvalue center and size of region
        [x_n, x_subseq] = find_subsequences(x_idx);
        [y_n, y_subseq] = find_subsequences(y_idx);
        total_num_comp = x_n*y_n;
        disp('total number of components ');
        disp(total_num_comp);
        
        pevT(idx,3)=total_num_comp;
        
        col_idx_base=3;
        for i=1:x_n
            for j=1:y_n
                fprintf('component id: %d, %d\n', i,j);
                %fprintf('lengths of subseq= %d, %d \n', length(x_subseq{i}), length(y_subseq{j}));
                xrange_lower_lim=x(1,x_subseq{i}(1));
                xrange_upper_lim=x(1,x_subseq{i}(length(x_subseq{i})));
                yrange_lower_lim=y(y_subseq{j}(1),1);
                yrange_upper_lim=y(y_subseq{j}(length(y_subseq{j}),1));
                fprintf('range: x(%f, %f), y(%f, %f) \n', ...
                    xrange_lower_lim, xrange_upper_lim, ...
                    yrange_lower_lim, yrange_upper_lim);
                fprintf('xrange length= %f, yrange length= %f\n', ...
                    xrange_upper_lim-xrange_lower_lim,...
                    yrange_upper_lim-yrange_lower_lim);
                
                xcenter=x(1,floor(median(x_subseq{i})));
                ycenter=y(floor(median(y_subseq{j})),1);
                fprintf('center= (%f, %f) \n', xcenter, ycenter);
                
                %recording the pseudoeigenvalues
                pevT(idx, col_idx_base+1)=xcenter;
                pevT(idx, col_idx_base+2)=ycenter;
                pevT(idx, col_idx_base+3)=xrange_upper_lim-xrange_lower_lim;
                pevT(idx, col_idx_base+4)=yrange_upper_lim-yrange_lower_lim;
                
                scatter3(ycenter, xcenter, ...
                    z(floor(median(x_subseq{i})), floor(median(y_subseq{j}))), 50, 'b', '*');
                col_idx_base=col_idx_base+4;

            end
        end

        %display('Press a key to continue\n');
        %while(~waitforbuttonpress)
        %end;
    end
end

%dataset({pevT 'tau', 'trial_number', '#comp', ...
%    'comp1:xcenter', 'comp1:ycenter', 'comp1:xlength', 'comp1:ylength', ...
%    'comp2:xcenter', 'comp2:ycenter', 'comp2:xlength', 'comp2:ylength', ...
%    'comp3:xcenter', 'comp3:ycenter', 'comp3:xlength', 'comp3:ylength', ...
%    'comp4:xcenter', 'comp4:ycenter', 'comp4:xlength', 'comp4:ylength', ...
%    });
