% Comments:
% May 30, 2015 - Porting the code from python to matlab
% Created by Abhishek K Dubey

clear; close all;
cmap = hsv(10);
grid_length = {'100', '200'};
distribution = {'Gaussian', 'Uniform'};
noise = {'10-1', '10-2', '10-3'};
data_dir='../../data/';
step=[.03, .015];

grid_length_scheme = input(...
    'Press 1 for grid length 100\nPress 2 for grid length 200\n? ');
distribution_scheme = input(...
    'Press 1 for Gaussian \nPress 2 for Uniform\n? ');
noise_scheme = input(...
    ['Press 1 for Noise level 10^-1\nPress 2 for noise level ' ...
    '10^-2\nPress 3 for noise level 10^-3\n? ']);

data_subdir = strcat(grid_length{grid_length_scheme}, '_',...
    distribution{distribution_scheme}, '_', noise{noise_scheme});
filename='/Data_p_spectra_sigm_matrix.txt'
data=load(strcat(data_dir, data_subdir, filename));
data=reshape(data, str2num(grid_length{grid_length_scheme})+1,...
    str2num(grid_length{grid_length_scheme})+1,10);

figure(1);
for i=1:10
    z=reshape(data(:,:,i), str2num(grid_length{grid_length_scheme})+1,...
        str2num(grid_length{grid_length_scheme})+1);
    [x,y] = meshgrid(-1.5:step(grid_length_scheme):1.501);
    scatter3(x(:),y(:),z(:), 5, cmap(i,:));
    xlabel('Real');
    ylabel('Imag');
    zlabel('Min of SinVal');
    title(strcat( num2str(i), ' trials'));
    hold on;
    display('Press a key to continue\n');
    while(~waitforbuttonpress)
    end;
end

%mean
figure(2);
avg=mean(data(:,:,:),3);
[x,y] = meshgrid(-1.5:step(grid_length_scheme):1.501);
scatter3(x(:),y(:),avg(:), 5, cmap(1,:));
xlabel('Real');
ylabel('Imag');
zlabel('Min of SinVal');
title('Average of 10 trails');
input('Press a key to continue\n');

%sd
figure(3);
sigma=std(data(:,:,:),0, 3);
[x,y] = meshgrid(-1.5:step(grid_length_scheme):1.501);
scatter3(x(:),y(:),sigma(:), 5, cmap(1,:));
xlabel('Real');
ylabel('Imag');
zlabel('Min of SinVal');
title('sd of 10 trails');
input('Press a key to continue\n');



