%
% Generate data on a 2d swiss roll
% June 10, 2015
% Created by Abhishek K Dubey
close all;
clear;

% Swissroll generation parameters
tracePhi   = 3*pi / 2;
traceAlpha = 1;
traceBeta  = 2;

widthAlpha = 0;
widthBeta  = 20;
numofnormalpoints=5;
normaldist=.1;

% numSamples = 5000;

numTrSurf  = 50;
numWdSurf  = 20;

fprintf( '\n   generating swiss roll manifold surface ...\n' )

traceSurf = traceAlpha + traceBeta * linspace( 0, 1, numTrSurf ) ;
traceSurf = tracePhi * traceSurf ; 
widthSurf = widthAlpha + widthBeta * linspace( 0, 1, numWdSurf );

x=traceSurf .* cos(traceSurf);
y=widthSurf;
[xxSurf, yySurf] = meshgrid( x, y);

z=traceSurf .* sin(traceSurf);
zzSurf = repmat( z, size(xxSurf,1), 1 );
ccSurf = repmat( traceSurf, size(xxSurf,1), 1 );

%dy = gradient(yySurf);
%dx = gradient(xxSurf);
%dz = gradient(zzSurf);

%normalizing
%dy = dy./sqrt(dy.^2+dx.^2+dz.^2);
%dx = dx./sqrt(dy.^2+dx.^2+dz.^2);
%dz = dz./sqrt(dy.^2+dx.^2+dz.^2);

[Nx, Ny, Nz] = surfnorm(xxSurf,yySurf,zzSurf);

%normalizing
Nx=-Nx./sqrt(Nx.^2+Ny.^2+Nz.^2);
Ny=-Ny./sqrt(Nx.^2+Ny.^2+Nz.^2);
Nz=-Nz./sqrt(Nx.^2+Ny.^2+Nz.^2);

fprintf( '\n   visualizing the swiss roll \n' );

figure(1)
hold on;
surf( xxSurf, yySurf, zzSurf, ccSurf);
quiver3(xxSurf, yySurf, zzSurf, Nx, Ny, Nz);
axis equal;
shading interp;
title( 'Swiss roll manifold (axis-parallel view)' );
xlabel( 'x' );
ylabel( 'y' );
zlabel( 'z' );
view(3);
hold off;

X = [xxSurf(:)'; yySurf(:)'; zzSurf(:)'];

for i=1:numofnormalpoints
    X = [X; 
    xxSurf(:)'+i*normaldist*Nx(:)';
    yySurf(:)'+i*normaldist*Ny(:)';
    zzSurf(:)'+i*normaldist*Nz(:)';
    xxSurf(:)'-i*normaldist*Nx(:)';
    yySurf(:)'-i*normaldist*Ny(:)';
    zzSurf(:)'-i*normaldist*Nz(:)';
    ];
end

figure(2);
scatter3( X(1,:)', X(2,:)', X(3,:)', ccSurf(:) );
%for i=1:numofnormalpoints
%    scatter3( xxSurf(:)+i*normaldist*Nx(:), yySurf(:)+i*normaldist*Ny(:), zzSurf(:)+i*normaldist*Nz(:), ccSurf(:) );
%end
