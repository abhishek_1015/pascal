function [ t, L ] = LorenzSys( paras, tab, xyz0 )
% 
%  Numerical solution for the Lorenz ODE system 
%   with parameters specified in PARAS ( >0 ) 
%   over time interval specified by TAB ( > 0 ) 
%   and initial values specified by XYZ0 
%
%  See a driver script in demo_LorenzSys.m 
%      
%  Numerical solution method : RK methd of 4/5 order  
%      built-in function ODE45(...)  
%  
%  Note: Lorenz used the parameter values as follows 
%            paras = [ 8/3, 28, 10 ] ;
%  but one shall take a look at the folloinw values 
%  of rho at leat 
%   rho \in { 1, 5, 9, 13, 17, 22, 23.75, 23.80, 28 } ; 

beta  = paras(1) ; 
rho   = paras(2) ; 
sigma = paras(3) ; 

% ... feqn is the set of ODEs : 
%     parameters : beta, rho and sigma 
%     time variable : t in adaptive steps 
%     spatial variables : L(:,1:3), [x,y,z] at time ticks in t 
% 

feqn = @(t,L) [ -sigma*L(1) + sigma*L(2) ; ...     % dx/dt 
                 rho*L(1) - L(2) - L(1)*L(3); ...  % dy/dt 
                -beta*L(3) + L(1)*L(2) ];          % dz/dt 
          
[t,L] = ode45( feqn, tab , xyz0 ) ;  
        % 'ode45' uses adaptive Runge-Kutta method 
        %  of 4th and 5th order to solve differential equations

return 

% -------------- Notes ---------------------------
% 
% Edward Lorenz's Atmospheric Convection (1963) 
% 
% dx/dt = sigma ( y - x ) 
% dy/dt = x( rho - z ) - y 
% dz/dt = xy - beta z 
%       over t \in [a,b] 
%       with the intial values [x(a), y(a), z(a) ]
% 
% Lorenz used the following parameter values 
%    beta = 8/3 ; rho = 28 ; sigma = 10 
%
% In general, (sigma, beta, rho) > 0 
% 
% 
