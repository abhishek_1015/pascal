% demo_LorenzSys.m 
% 
% Demonstrate how to use function LorenzSys(...) 
% Observe the sensitivity to the change in the parameters 
% 

clear all
close all 


visFlag = 1 ;

tab   = [0, 100 ] ;             % time interval  
xyz0  = [1, 1, 1 ];             % initial values  


beta  = 8/3 ; 
sigma = 10  ; 

strParas2 = sprintf( 'beta = %.4g, sigma = %.4g ', beta, sigma ); 

rhoS  = [ 1, 5, 9, 13, 17, 22, 23.75, 23.80, 28 ]; 

for j = 1 : length( rhoS ) 
    
  rho      = rhoS(j) ; 
  
  
  paras = [ beta, rho, sigma ] ;    % system parameters 

  [t,L] = LorenzSys( paras, tab, xyz0 ) ;

  % ... visualization 

  if visFlag 
   strParas = [ strParas2, sprintf( ' rho = %.4g ', rho ) ]; 
   
   figure 
   plot3( L(:,1),L(:,2),L(:,3),'b.') 
   title( strParas ); 
   view( [-135, 10] ) ;
   
   xlabel( 'x' ) 
   ylabel( 'y' ) 
   zlabel( 'z' ) 
   grid on 
   rotate3d 
   
  end 
end % loop over rho values 


%---------------------
% Xiaobai Sun 
% Duke CS 
% June 4, 2015 
% -------------------
